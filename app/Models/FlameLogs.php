<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlameLogs extends Model
{

    protected $fillable = [
        'cage_id',
        'temperature',
        'status'
    ];
}
