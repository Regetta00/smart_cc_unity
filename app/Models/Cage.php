<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cage extends Model
{
    protected $fillable = [
        'user_id',
        'lock_status'
    ];

    public function flameLogs()
    {
        return $this->hasOne('App\Models\FlameLogs', 'cage_id', 'id')->latest();
    }

    public function waterLogs()
    {
        return $this->hasOne('App\Models\WaterLogs', 'cage_id', 'id')->latest();
    }
}
