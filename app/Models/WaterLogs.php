<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WaterLogs extends Model
{
    protected $fillable = [
        'cage_id',
        'water_level',
        'status'
    ];
}
