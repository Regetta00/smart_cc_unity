<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Cage;
use App\Models\WaterLogs;
use App\Models\FlameLogs;
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{
    public function register(Request $request) {
        $data = [
            'name' => $request->name,
            'collar_no' => $request->collar_no,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ];

        $user = User::create($data);
        $cageData = [
        'user_id' => $user->id,
        'lock_status' => 'Locked'
        ];
        $cage = Cage::create($cageData);
        return $user;
    }


    public function login(Request $request) {
        $credentials = [
            'email' => $request->email,
            'password'  => $request->password
        ];
        // dd($credentials,Auth::attempt($credentials));
        if(Auth::attempt($credentials)){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(compact('success', 'user'));
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function fetchCageStats() {
        $cage = User::get();
        return $cage;
    }

    public function lockUnlock(Request $request) {
        $keyword = $request->keyword;
        $cage = Cage::where('user_id',$request->user_id)->first();
        $cage->lock_status = $keyword;
        $cage->save();
    }
}
